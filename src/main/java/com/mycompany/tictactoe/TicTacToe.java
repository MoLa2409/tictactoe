package com.mycompany.tictactoe;

import javax.swing.JFrame;

public class TicTacToe {

    public static void main(String[] args) {
        JFrame frame = new JFrame();

        frame.setSize(500, 500);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);

        TicTacToeGUI gui = new TicTacToeGUI();
        frame.setContentPane(gui);
        
        
        frame.setVisible(true);
    }

}
