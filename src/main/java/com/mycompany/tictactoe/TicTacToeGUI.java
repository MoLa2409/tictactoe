package com.mycompany.tictactoe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class TicTacToeGUI extends JPanel implements ActionListener {

    JLabel spieler1;
    JLabel spieler2;

    JButton btn_1;
    JButton btn_2;
    JButton btn_3;
    JButton btn_4;
    JButton btn_5;
    JButton btn_6;
    JButton btn_7;
    JButton btn_8;
    JButton btn_9;

    TicTacToeLogik logik = new TicTacToeLogik();

    public TicTacToeGUI() {

        setLayout(null);

        spieler1 = new JLabel();
        spieler1.setBounds(20, 20, 170, 50);
        spieler1.setText("SPIELER 1");
        spieler1.setBorder(new LineBorder(Color.BLACK, 3));
        this.add(spieler1);

        spieler2 = new JLabel();
        spieler2.setBounds(270, 20, 170, 50);
        spieler2.setText("SPIELER 2");
        spieler2.setBorder(new LineBorder(Color.BLACK, 3));
        this.add(spieler2);

        btn_1 = new JButton();
        btn_1.setBounds(40, 100, 75, 75);
        this.add(btn_1);

        btn_2 = new JButton();
        btn_2.setBounds(200, 100, 75, 75);
        this.add(btn_2);

        btn_3 = new JButton();
        btn_3.setBounds(360, 100, 75, 75);
        this.add(btn_3);

        btn_4 = new JButton();
        btn_4.setBounds(40, 230, 75, 75);
        this.add(btn_4);

        btn_5 = new JButton();
        btn_5.setBounds(200, 230, 75, 75);
        this.add(btn_5);

        btn_6 = new JButton();
        btn_6.setBounds(360, 230, 75, 75);
        this.add(btn_6);

        btn_7 = new JButton();
        btn_7.setBounds(40, 360, 75, 75);
        this.add(btn_7);

        btn_8 = new JButton();
        btn_8.setBounds(200, 360, 75, 75);
        this.add(btn_8);

        btn_9 = new JButton();
        btn_9.setBounds(360, 360, 75, 75);
        this.add(btn_9);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        int random = logik.random();

        if (random % 2 == 0 && e.getSource().equals(btn_1)) {
            

        }
    }

}
